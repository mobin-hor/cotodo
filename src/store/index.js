import Vue from 'vue'
import Vuex from 'vuex'
import { v4 as uuidv4 } from 'uuid';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    projects:[],
    PopUp:false,
  },

  getters:{
    projects : (state) => state.projects,
    PopUp: (state) => state.PopUp,
    Totals:(state)=>{
      let Completed=0 , OnGoing=0 , OverDue=0
      let CompletedPercent , OnGoingPercent , OverDuePercent
      state.projects.filter(e => {
        if(e.status === 'Completed'){
          Completed++;
        }
      })
      state.projects.filter(e => {
        if(e.status === 'OnGoing'){
          OnGoing++;
        }
      })
      state.projects.filter(e => {
        if(e.status === 'OverDue'){
          OverDue++;
        }
      })
      if(Completed > 0){
        CompletedPercent = Math.floor((Completed / state.projects.length)*100);
      }else
        CompletedPercent=0;
        
      if(OnGoing > 0){
        OnGoingPercent = Math.floor((OnGoing / state.projects.length)*100);
      }else
      OnGoingPercent=0;
      
      if(OverDue > 0){
        OverDuePercent = Math.floor((OverDue / state.projects.length)*100);
      }else
      OverDuePercent=0;
      
      
      
     
      return{
        Completed,
        OnGoing,
        OverDue,
        CompletedPercent,
        OnGoingPercent,
        OverDuePercent,
      }
    }
  },

  mutations: {

    AddNewProject(state , payload){
      let project = {
          title : payload.title,
          DueTime : payload.DueTime,
          status:'OnGoing',
          progress: 0,
          id : uuidv4(),
          comments:[]
      }
        state.projects.push(project)
    },

    DeleteProject(state , id){

      let target = state.projects.filter(e=>{
        return (e.id!==id)
      })
      state.projects = target;
    },

    AddComment(state , payload){

      console.log(payload.id , payload.comment)
      let target = state.projects.filter(e=>{
        return (e.id === payload.id)
      })
      target[0].comments.push(payload.comment)
    },

    ProjectProgress(state , id){
      let target = state.projects.filter(e=>{
        return (e.id === id)
      })
      console.log(target[0])
      if(target[0].progress < 100)
        target[0].progress += 10;
      if(target[0].progress === 100){
        target[0].status = 'Completed'
      }
    },
    ProjectRegress(state , id){
      let target = state.projects.filter(e=>{
        return (e.id === id);
      })
      if(target[0].progress === 100){
        target[0].status = 'OnGoing'
      }
      if(target[0].progress >= 10){
        target[0].progress -= 10;
      }

    }
  },
  actions: {


  },
  modules: {


  }
})
